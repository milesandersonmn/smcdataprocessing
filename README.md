# SMC Data Processing
Step by step process to convert non-model organisms genomic data from BAM files to Multihetsep format. Various SMC based methods use the Multihetsep format as input (eSMC2, MSMC, MSMC2, etc.) However, a detailed description of the data processing for non-model organism data is difficult to find. This gitlab project will, hopefully remedy this problem in part.


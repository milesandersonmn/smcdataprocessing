# Software requirements
Proceeding through the tutorial will require the following softwares:

* GATK 4.1.8.1 or higher
* Bcftools
* Bedops
* Bedtools

## Steps

Proceed through the tutorials in the following order:

1. HaplotypeCaller
2. VCF2Mask/GenerateMultihetsep
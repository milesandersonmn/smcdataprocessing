# HaplotypeCaller

## Why use HaplotypeCaller?

SMC based methods work by moving along a pair of haplotypes and using the information of where a segregating site (SNP) is located and the number of base pairs that are homozygous reference calls between segregating sites. With this information an SMC method can estimate the time to most recent common ancestor (TMRCA) for a given length of sequence. Sequences that share a long block of homozygosity are assumed to share a recent common ancestor, and sequences of shorter length with high heterzygosity are assumed to share an older common ancestor.

However, the most common format for population genetic data is the variant call format (VCF.) This format generally only has information on the positions in a genome where a variant is present and one or more of the haplotypes present contains said variant. While VCFs are convenient for data storage and many different analyses, they do not contain information at sites which do not have a variant present. This makes the use of a VCF for SMC based methods rather tricky because the lack of a variant call in a VCF does not necessarily mean the lack of variation at that position in the genome. It is possible that positions not present in the VCF are indeed simply homozygous reference calls. However, it is also possible these positions are inaccessible to the sequencing technique employed in an experiment, could not be properly aligned, contained some structural variation, etc.

Importantly, this will bias the inference of SMC methods because if we assume that positions that are not in the VCF are homozygous reference alleles rather than actually being positions where reliable base calling was not possible, then we assume distances between segregating sites without intervening variants is longer than it probably is. So the SMC will estimate a short TMRCA as if it was analyzing a long sequence of homozygosity.

Instead we need a data format with information at all genomic positions and whether those positions are variant, invariant, or do not have reliable base calls. Luckily for us, such a format exists and HaplotypeCaller can generate this for us: the GVCF.

## GVCF

The GVCF is a file format like a VCF but it contains information for all sites in the genome, variant or invariant. This is good for SMC methods but comes at the cost of ballooning the size of a variant call file. A 50 Mb chromosome would create a GVCF of 50 million lines (not including the lines in the header.) So for this reason, it's important to understand the size of the genome for the species you are working with as well as the constraints of your computing resources. For this reason we will only run the following workflow on a single chromosome. 

To find the names of the chromosomes for the commands used later, use the ```grep``` software on your reference fasta file:

```
grep "^>" <yourReference.fasta> | less
```
This will search for all lines in the fasta which contain the ">" symbol (these will be the names of your chromosomes.)

# Using HaplotypeCaller

Now that we understand why to use HaplotypeCaller, we can actually use it.

You will need:

* Your BAM files
* A reference genome in fasta format

Store your BAM files in a directory called "bamFiles" and your reference fasta in a directory called "reference."

Create a directory for you VCFs that are output from HaplotypeCaller.

```
mkdir VCFs
```

Now we will use the following command to use HaplotypeCaller:

```
gatk --java-options "-Xmx4g" HaplotypeCaller \
-R reference/reference.fasta \
-I bamFiles/sample1.bam \
-O VCFs/sample1.Chr2.g.vcf.gz \
-ERC BP_RESOLUTION \
--output-mode EMIT_ALL_CONFIDENT_SITES \
-L Chr2
```

So the -R flag inputs our reference genome, the -I inputs our BAM file, and the -O outputs our GVCF to the VCFs directory. The remaining flags are special arguments we need specifically for processing data for the SMC methods.

First is the -ERC flag. This stands for "emit reference confidence." This can accept an argument of NONE, BP_RESOLUTION, or GVCF. BP_RESOLUTION creates an entry for every site in the genome, whereas GVCF will compress sequential homozygous reference base calls into a block, so long stretches of homozygosity will only need one line in the file compared to BP_RESOLUTION. However, we need the BP_RESOLUTION argument for the ease of processing downstream. 

--output-mode with "EMIT_ALL_CONFIDENT_SITES" will allow us to create an entry for every base call, variant or invariant, with a confident reference site.

-L will restrict the process to a set portion of the genome. In this case chromosome 2, but you can use whichever chromosome or contig you like.

If you have enough computational resources, you can also add the flag ```--native-pair-hmm-threads 8``` to speed up the process. But make sure you do not exceed the number of threads available on your machine.

Repeat this process for as many sample BAM files as you wish to include in the analysis. I would recommend not exceeding 5 diploid samples. This tutorial will use 3 samples. Once you have the desired GVCFs, you can proceed to the second part of the tutorial in, which we will create the "mask" file using the VCFs.